# Maven Project Archetype

This module defines an archetype to instantiate a new project with proper Gazelle structure.
It allows to instantiate a Gazelle project to parent your modules.

## Goals

- Generate a new project with a pom file
- pom should contain identifiers, nexus connection, compiler plugin, sonar, test plugin and library.

## Properties

Here is the list of properties that can be used to generate a project with this Archetype :

| Property Name | Default Value   | Usage                                                                                                                                            |
|---------------|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| projectPath   | None            | Path of the project on the git repository. No need to include "gazelle/" thats is added by default.                                              |
| groupId       | net.ihe.gazelle | Group ID for maven pom.xml                                                                                                                       |
| artifactId    | my-module       | Part of maven pom.xml artifactId (will be prefix.**artifactId**). Also used as folder name for your new module                                   |
| prefix        | lib             | Prefix for the module. Will be used in maven artifactId (will be **prefix**.artifactId). Also used to name package at net.ihe.gazelle.**prefix** |
| projectName   | My Module       | Project Name for the maven module.                                                                                                               | 
| version       | 1.0.0-SNAPSHOT  | Version of the module, and of its parent.                                                                                                        |


## How to generate a new Module

To generate a new project with this archetype, go to the target folder of your choice :

```cd my/target/folder```

Then use the following command to generate your module :

```mvn archetype:generate -DarchetypeGroupId=net.ihe.gazelle -DarchetypeArtifactId=arch.project -DarchetypeVersion=1.0.6 -DprojectPath="library/search.git" -DgroupId=net.ihe.gazelle -Dprefix=lib -DartifactId=search -DprojectName="Search" -Dversion=1.0.0-SNAPSHOT ```

First 3 parameters are mandatory to specify which archetype you are using and in which version.
Following parameters are optional, as default values are defined. If they are not defined in the command, default values will be used.
*projectPath* property defines no default value. This means it SHALL be defined in the command.